int mfmod_ldap_search(long count, MFMOD_PARAM *p, MFMOD_PARAM *r);
int mfmod_ldap_search_result_next_entry(long count, MFMOD_PARAM *p, MFMOD_PARAM *r);

struct mfmod_response_attr {
	char *name;
	size_t count;
	char **values;
};

struct mfmod_response {
	LDAP *ld;
	LDAPMessage *res;
	LDAPMessage *cur;
	char *dn;
	size_t nattr;
	struct mfmod_response_attr *attr;
};

struct mfmod_response *response_get(int n);
struct mfmod_response_attr *response_attr_get(struct mfmod_response *resp, char const *name);
void free_response(long n);
