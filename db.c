/* This file is part of mfmod_ldap.
   Copyright (C) 2023-2024 Sergey Poznyakoff

   Mfmod_ldap is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Mfmod_ldap is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with mfmod_ldap.  If not, see <http://www.gnu.org/licenses/>. */

#include "config.h"
#include <stdlib.h>
#include <ctype.h>
#include <mailfromd/mfmod.h>
#include <mailfromd/exceptions.h>
#include <mailutils/mailutils.h>
#include <mailutils/dbm.h>
#include <mailutils/sys/dbm.h>
#include <assert.h>
#include <ldap.h>
#include <mfmod_ldap.h>

/*
 * String formatter definitions.
 */

/* Formatter segment types. */
enum {
	segm_literal,
	segm_attribute
};

/* Segment structure. */
struct form_segm {
	struct form_segm *next;   /* Link to next segment. */
	int type;                 /* Segment type. */
	union {
		char *text;       /* For type == segm_literal */
		size_t attr;      /* For type == segm_attribute.  This is
				     the index of the atv[] entry in the
				     owner ldap_db structure. */
	};
};

/* A parsed out format. */
struct ldap_format {
	struct form_segm *form_head, *form_tail;
};

/* LDAP database structure. */
struct ldap_db {
	char *name;    /* Name of the database. */
	char *base_dn; /* Base DN.  Emtpy string means use default. */
	size_t atc;    /* Number of attributes used in formats below. */
	size_t atm;    /* Max. number of attributes (capacity of atv). */
	char **atv;    /* Attribute names. */
	struct ldap_format get_filter;
		       /* Format for generating LDAP filter for use by
			  _dbm_fetch routine.  Must use exactly one
			  argument (attribute). */
	char *foreach_filter;
		       /* Filter for use in sequential access methods. */
	struct ldap_format response;
		       /* Format for converting LDAP response to conten
			  (return) string, */
	struct ldap_format key_fmt;
		       /* Format for converting LDAP response to key string.
			  Returned string must be a valid input to
			  mu_dbm_fetch. */
	char *errstr;  /* Last error in string representation. */
	int rd;        /* Response number (see alloc_response et al.) used
			  diring sequential access. */
};

/* Storage for LDAP database definitions. */
static struct ldap_db *dbv;  /* Array of definitions. */
static size_t num_db;        /* Number of elements used. */
static size_t max_db;        /* Actual number of slots in dbv. */

/* Find the database with the supplied name. */
struct ldap_db *
find_db(char const *name)
{
	size_t i;

	for (i = 0; i < num_db; i++) {
		if (dbv[i].name && strcmp(dbv[i].name, name) == 0)
			return dbv + i;
	}
	return NULL;
}

/*
 * Allocate new database.  Return NULL if the database with that name
 * already exists.
 */
struct ldap_db *
alloc_db(char const *name)
{
	size_t i;
	ssize_t first_free = -1;

	for (i = 0; i < num_db; i++) {
		if (dbv[i].name == NULL) {
			if (first_free == -1)
				first_free = i;
		} else if (strcmp(dbv[i].name, name) == 0)
			return NULL;
	}
	if (first_free == -1) {
		if (num_db == max_db)
			dbv = mu_2nrealloc(dbv, &max_db, sizeof(dbv[0]));
		first_free = num_db;
		num_db++;
	}
	memset(&dbv[first_free], 0, sizeof(dbv[first_free]));
	dbv[first_free].name = mu_strdup(name);
	return dbv + first_free;
}

/*
 * Find in db->atv attribute with the given name (len bytes long).  If not
 * found, insert new name.  Return index of the name in the array.
 */
static ssize_t
find_attr_name(struct ldap_db *db, char const *name, int len)
{
	size_t i;
	for (i = 0; i < db->atc; i++) {
		if (strlen(db->atv[i]) == len && memcmp(db->atv[i], name, len) == 0)
			return i;
	}
	if (db->atc == db->atm)
		db->atv = mu_2nrealloc(db->atv, &db->atm, sizeof(db->atv));
	i = db->atc++;
	db->atv[i] = mu_alloc(len + 1);
	memcpy(db->atv[i], name, len);
	db->atv[i][len] = 0;
	return i;
}

static inline int
is_ident_start(int c)
{
	return (isascii(c) && isalpha(c)) || c == '_';
}

static inline int
is_ident_cont(int c)
{
	return is_ident_start(c) || isdigit(c);
}

/*
 * Extract attribute name from the input string.  On success,
 * point *NAME to the beginning of the name and initialize
 * *NAMELEN with the name length.
 * Advance STR past the attribute reference and return this new
 * position.
 *
 * On input, STR must points to a fragment of input string beginning
 * with '$'.
 */
static char const *
getname(char const *str, char const **name, size_t *namelen)
{
	int i;

	if (str[1] == '{') {
		if (!is_ident_start(str[2]))
			return NULL;
		for (i = 3; str[i] != '}'; i++) {
			if (str[i] == 0 || !is_ident_cont(str[i]))
				return NULL;
		}
		*name = str + 2;
		*namelen = i - 2;
		str += i + 1;
	} else {
		if (!is_ident_start(str[1]))
			return NULL;
		for (i = 2; is_ident_cont(str[i]); i++) {
			if (str[i] == 0)
				return NULL;
		}
		*name = str + 1;
		*namelen = i - 1;
		str += i;
	}
	return str;
}

/* Allocate new format segment of given TYPE in FMT. */
static struct form_segm *
new_form_segm(struct ldap_format *fmt, int type)
{
	struct form_segm *segm = mu_alloc(sizeof(*segm));
	segm->next = NULL;
	segm->type = type;
	if (fmt->form_tail)
		fmt->form_tail->next = segm;
	else
		fmt->form_head = segm;
	fmt->form_tail = segm;
	return segm;
}

/*
 * Append a literal segment to format object FMT.  STR points to the
 * literal text and LEN gives number of bytes in STR.
 */
static void
format_add_literal(struct ldap_format *fmt, char const *str, size_t len)
{
	if (len > 0) {
		struct form_segm *segm = new_form_segm(fmt, segm_literal);
		segm->text = mu_alloc(len + 1);
		memcpy(segm->text, str, len);
		segm->text[len] = 0;
	}
}

/*
 * Append an attribute segment to the format object FMT of the database
 * structure DB.
 * NAME points to the attribute name, LEN gives its length.
 * If necessary, new attribute name will be allocated and stored in db->atv.
 */
static void
format_add_attribute(struct ldap_db *db, struct ldap_format *fmt, char const *name, size_t len)
{
	struct form_segm *segm = new_form_segm(fmt, segm_attribute);
	segm->attr = find_attr_name(db, name, len);
}

/*
 * Scan format string INPUT.  Create format builder in FMT.  Allocate
 * attribute names in DB.
 */
static void
scan_format(struct ldap_db *db, struct ldap_format *fmt, char *input)
{
	char const *start = input;
	char const *p;

	p = start;
	while (*p) {
		if (*p == '\\')
			p += 2;
		else if (*p == '$') {
			char const *name;
			size_t len;
			char const *end = getname(p, &name, &len);

			if (end == NULL) {
				p++;
			} else {
				format_add_literal(fmt, start, p - start);
				format_add_attribute(db, fmt, name, len);
				start = p = end;
			}
		} else
			p++;
	}
	format_add_literal(fmt, start, p - start);
}

static void
free_format(struct ldap_format *fmt)
{
	while (fmt->form_head) {
		struct form_segm *next = fmt->form_head;
		if (fmt->form_head->type == segm_literal)
			free(fmt->form_head->text);
		free(fmt->form_head);
		fmt->form_head = next;
	}
}

static void
free_db(struct ldap_db *db)
{
	size_t i;

	free(db->name);
	db->name = NULL;
	free(db->base_dn);
	db->base_dn = NULL;
	free(db->errstr);
	db->errstr = NULL;
	for (i = 0; i < db->atc; i++)
		free(db->atv[i]);
	free(db->atv);
	free_format(&db->get_filter);
	free(db->foreach_filter);
	db->foreach_filter = NULL;
	free_format(&db->response);
}

struct stringbuf {
	char *base;
	size_t size;
	size_t len;
};

#define STRINGBUF_INITIALIZER { NULL, 0, 0 }

static void
stringbuf_add(struct stringbuf *sb, char const *str, size_t len)
{
	while (sb->size < sb->len + len)
		sb->base = mu_2nrealloc(sb->base, &sb->size, 1);
	memcpy(sb->base + sb->len, str, len);
	sb->len += len;
}

static void
stringbuf_add_string(struct stringbuf *sb, char const *str)
{
	stringbuf_add(sb, str, strlen(str));
}

static char *
stringbuf_finish(struct stringbuf *sb)
{
	stringbuf_add(sb, "", 1);
	return sb->base;
}

/*
 * Expand format FMT.  VALUES must contain non-NULL values to substitute
 * for attribute names.
 */
static char *
expand_format(struct ldap_format *fmt, char **values)
{
	struct stringbuf sb = STRINGBUF_INITIALIZER;
	struct form_segm *segm;

	for (segm = fmt->form_head; segm; segm = segm->next) {
		if (segm->type == segm_literal)
			stringbuf_add_string(&sb, segm->text);
		else if (values[segm->attr])
			stringbuf_add_string(&sb, values[segm->attr]);
	}
	return stringbuf_finish(&sb);
}


/*
 * A definition of Mailutils DBM object.
 */
static int
ldb_file_safety(mu_dbm_file_t mdb, int mode, uid_t owner)
{
	return 0;
}

static int
ldb_get_fd(mu_dbm_file_t mdb, int *pag, int *dir)
{
	return ENOSYS;
}

static int
ldb_open(mu_dbm_file_t mdb, int flags, int mode)
{
	struct ldap_db *ldb = find_db(mdb->db_name);
	if (!ldb)
		return MU_ERR_NOENT;
	mdb->db_descr = ldb;
	return 0;
}

static void
ldb_free_response(struct ldap_db *ldb)
{
	if (ldb->rd >= 0) {
		free_response(ldb->rd);
		ldb->rd = -1;
	}
}

static int
ldb_close(mu_dbm_file_t db)
{
	struct ldap_db *ldb = db->db_descr;
	ldb_free_response(ldb);
	return 0;
}

static void
db_free_error(struct ldap_db *ldb)
{
	if (ldb->errstr) {
		free(ldb->errstr);
		ldb->errstr = NULL;
	}
}

static int
response_to_datum(mu_dbm_file_t db, int rd,
		  struct ldap_format *format,
		  struct mu_dbm_datum *ret)
{
	struct ldap_db *ldb = db->db_descr;
	struct mfmod_response *resp = response_get(rd);
	char **argv;
	size_t i;
	char *result;

	if (ldap_count_entries(resp->ld, resp->cur) == 0) {
		free_response(rd);
		return MU_ERR_NOENT;
	}

	argv = mu_calloc(ldb->atc, sizeof(argv[0]));
	for (i = 0; i < ldb->atc; i++) {
		struct mfmod_response_attr *attr = response_attr_get(resp, ldb->atv[i]);
		if (attr && attr->count > 0)
			argv[i] = attr->values[0];
		else
			argv[i] = "";
	}

	result = expand_format(format, argv);
	free(argv);

	ret->mu_dptr = result;
	ret->mu_dsize = strlen(result);
	ret->mu_sys = db->db_sys;
	return 0;
}

static int
db_fetch_internal(mu_dbm_file_t db, char const *filter,
		  struct ldap_format *format,
		  struct mu_dbm_datum *ret, int *prd)
{
	struct ldap_db *ldb = db->db_descr;
	MFMOD_PARAM *spm, r;
	int res;
	size_t i;

	db_free_error(ldb);

	spm = mu_calloc(ldb->atc + 2, sizeof(spm[0]));

	spm[0].type = mfmod_string;
	spm[0].string = ldb->base_dn;

	spm[1].type = mfmod_string;
	spm[1].string = (char*)filter;

	for (i = 0; i < ldb->atc; i++) {
		spm[2+i].type = mfmod_string;
		spm[2+i].string = ldb->atv[i];
	}

	res = mfmod_ldap_search(i + 2, spm, &r);

	free(spm);

	if (res != 0) {
		if (r.type == mfmod_string)
			ldb->errstr = r.string;
		else
			ldb->errstr = mu_strdup("failure");
		return MU_ERR_FAILURE;
	}

	assert(r.type == mfmod_number);
	res = response_to_datum(db, r.number, format, ret);
	if (res == 0 && prd)
		*prd = r.number;
	else
		free_response(r.number);
	return res;
}

static int
ldb_fetch(mu_dbm_file_t db, struct mu_dbm_datum const *key,
	  struct mu_dbm_datum *ret)
{
	struct ldap_db *ldb = db->db_descr;
	char *values[1];
	char *filter;
	int rc;

	values[0] = mu_alloc(key->mu_dsize + 1);
	memcpy(values[0], key->mu_dptr, key->mu_dsize);
	values[0][key->mu_dsize] = 0;
	filter = expand_format(&ldb->get_filter, values);
	free(values[0]);
	rc = db_fetch_internal(db, filter, &ldb->response, ret, NULL);
	free(filter);
	return rc;
}

static int
ldb_firstkey(mu_dbm_file_t db, struct mu_dbm_datum *ret)
{
	struct ldap_db *ldb = db->db_descr;
	return db_fetch_internal(db, ldb->foreach_filter, &ldb->key_fmt, ret, &ldb->rd);
}

static int
ldb_nextkey(mu_dbm_file_t db, struct mu_dbm_datum *ret)
{
	struct ldap_db *ldb = db->db_descr;
	MFMOD_PARAM p, r;
	int rc;

	if (ldb->rd == -1)
		return EINVAL;

	p.type = mfmod_number;
	p.number = ldb->rd;
	rc = mfmod_ldap_search_result_next_entry(1, &p, &r);
	if (rc) {
		ldb_free_response(ldb);
		if (r.type == mfmod_string)
			ldb->errstr = r.string;
		else
			ldb->errstr = mu_strdup("failure");
		return MU_ERR_FAILURE;
	}

	rc = response_to_datum(db, ldb->rd, &ldb->key_fmt, ret);
	if (rc) {
		/* response_to_datum frees the response on error */
		ldb->rd = -1;
	}
	return rc;
}

static char const *
ldb_strerror(mu_dbm_file_t db)
{
	struct ldap_db *ldb = db->db_descr;
	return ldb->errstr;
}

#ifdef HAVE_STRUCT_MU_DBM_IMPL__DBM_BREAK
static int
ldb_break(mu_dbm_file_t db)
{
	struct ldap_db *ldb = db->db_descr;
	ldb_free_response(ldb);
}
#endif

static struct mu_dbm_impl _mu_dbm_ldap = {
  ._dbm_name        = "ldap",
  ._dbm_file_safety = ldb_file_safety,
  ._dbm_get_fd      = ldb_get_fd,
  ._dbm_open        = ldb_open,
  ._dbm_close       = ldb_close,
  ._dbm_fetch       = ldb_fetch,
  ._dbm_firstkey    = ldb_firstkey,
  ._dbm_nextkey     = ldb_nextkey,
  ._dbm_strerror    = ldb_strerror,
#ifdef HAVE_STRUCT_MU_DBM_IMPL__DBM_BREAK
  ._dbm_break       = ldb_break
#endif
};

static int
verify_format(struct ldap_format *fmt)
{
	struct form_segm *s;
	int n = -1;
	for (s = fmt->form_head; s; s = s->next) {
		if (s->type == segm_attribute) {
			if (n == -1)
				n = s->attr;
			else if (n != s->attr)
				return 1;
		}
	}
	return 0;
}

/*
 * mfmod_ldap_define_db
 * --------------------
 * Defines a "database" using LDAP storage backend.
 *
 * Input parameters:
 *   p[0]    string      Database name.  The database will be accessible
 *                       as "ldap://NAME".
 *   p[1]    string      Base DN.
 *   p[2]    string      "Get" filter expression.
 *   p[3]    string      Format string for converting LDAP replies into
 *                       MFL return string values.
 *   p[4]    string      LDAP filter for sequential access to the database.
 *   p[5]    string      Format string for converting returned LDAP objects
 *                       back into DBM key strings.
 * Return:
 *   none
 * Exceptions:
 *   e_exists - A database with that name already exists.
 *   e_inval  - Too many attribute names referenced in p[2].
 */
int
mfmod_ldap_define_db(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	struct ldap_db *db;

	ASSERT_ARGCOUNT(r, count, 6);
	ASSERT_ARGTYPE(p, r, 0, mfmod_string);
	ASSERT_ARGTYPE(p, r, 1, mfmod_string);
	ASSERT_ARGTYPE(p, r, 2, mfmod_string);
	ASSERT_ARGTYPE(p, r, 3, mfmod_string);
	ASSERT_ARGTYPE(p, r, 4, mfmod_string);
	ASSERT_ARGTYPE(p, r, 5, mfmod_string);

	mu_dbm_register(&_mu_dbm_ldap);

	db = alloc_db(p[0].string);
	if (!db) {
		return mfmod_error(r, mfe_exists,
				   "database %s already defined", p[0].string);
	}
	db->base_dn = mu_strdup(p[1].string);
	scan_format(db, &db->get_filter, p[2].string);
	if (verify_format(&db->get_filter)) {
		free_db(db);
		return mfmod_error(r, mfe_inval,
				   "too many attributes in filter");
	}
	scan_format(db, &db->response, p[3].string);
	db->foreach_filter = mu_strdup(p[4].string);
	scan_format(db, &db->key_fmt, p[5].string);

	r->type = mfmod_number;
	r->number = 0;
	return 0;
}
