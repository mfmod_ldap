/* This file is part of mfmod_ldap.
   Copyright (C) 2022-2024 Sergey Poznyakoff

   Mfmod_ldap is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Mfmod_ldap is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with mfmod_ldap.  If not, see <http://www.gnu.org/licenses/>. */
#include "config.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <ldap.h>
#include <limits.h>
#include <mailfromd/mfmod.h>
#include <mailfromd/exceptions.h>
#include <mailutils/mailutils.h>
#include <mfmod_ldap.h>

static int debug_level;
static int ldap_debug_level;

# define DEBUG(level, fmt, ...)						\
	do {								\
		if (debug_level >= level)				\
			debug_printer (__func__, fmt, __VA_ARGS__);	\
	} while (0)

static void
debug_printer(char const *funcname, char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	mu_diag_printf (MU_DIAG_DEBUG, "DEBUG: %s: ", funcname);
	mu_diag_cont_vprintf (fmt, ap);
	mu_diag_cont_printf ("%c", '\n');
	va_end(ap);
}

struct ldap_param {
	char *name;
	char *value;
	struct ldap_param *next;
};

static void
ldap_param_free(struct ldap_param *head)
{
	while (head) {
		struct ldap_param *next = head->next;
		free(head->name);
		free(head->value);
		free(head);
		head = next;
	}
}

static char *
ldap_param_get(struct ldap_param *head, char const *name)
{
	for (; head; head = head->next)
		if (strcasecmp(head->name, name) == 0)
			return head->value;
	return NULL;
}

static struct ldap_param *
ldap_read_config_file(char const *file_name)
{
	FILE *fp;
	char buf[BUFSIZ];
	char *s;
	struct ldap_param *p_head = NULL, *p_tail = NULL, *p;
	unsigned lineno;
	int err = 0;

	fp = fopen(file_name, "r");
	if (!fp)
		return NULL;

	lineno = 0;
	while ((s = fgets(buf, sizeof buf, fp)) != NULL) {
		size_t len;

		while (*s != '\n' && isspace(*s))
			s++;

		lineno++;
		len = strlen(s);
		if (len == 0 || s[len-1] != '\n') {
			int c;
			mu_error("%s:%u: line too long; skipped", file_name,
				    lineno);
			while ((c = fgetc(fp)) != EOF && c != '\n')
				;
			continue;
		}
		s[len-1] = 0;

		if (*s == '#' || *s == 0)
			continue;

		len = strcspn(s, " \t");
		if (s[len] == 0) {
			mu_error("%s:%u: syntax error", file_name, lineno);
			continue;
		}

		if ((p = calloc(1, sizeof(*p))) == NULL) {
			err = errno;
			break;
		}

		p->next = NULL;
		if (p_tail)
			p_tail->next = p;
		else
			p_head = p;
		p_tail = p;
		if ((p->name = malloc(len + 1)) == NULL) {
			err = errno;
			break;
		}
		memcpy(p->name, s, len);
		p->name[len] = 0;

		s += len;
		while (*s && isspace(*s))
			s++;
		if ((p->value = strdup(s)) == NULL) {
			err = errno;
			break;
		}
	}

	if (err == 0 && ferror(fp))
		err = EIO;

	if (err) {
		ldap_param_free(p_head);
		p_head = NULL;
		errno = err;
	}

	return p_head;
}

static char default_ldapconfpath[] = "/etc:/etc/ldap:/etc/openldap";
static char *ldapconfpath;
static char *ldapconffile = "ldap.conf";

static char *
ldap_config_path(void)
{
	return ldapconfpath ? ldapconfpath : default_ldapconfpath;
}

static struct ldap_param *
mfmod_ldap_conf_parse(char **file_name)
{
	static char const *path;
	struct ldap_param *ret = NULL;

	char *fbuf = NULL;
	size_t fbufsize = 0;
	size_t d = strlen(ldapconffile) + 2;
	int err = ENOENT;

	path = ldap_config_path();
	while (*path) {
		struct stat st;
		size_t len, n;
		char const *p = strchr(path, ':');

		if (p) {
			len = p - path;
			p++;
		} else {
			len = strlen(path);
			p = path + len;
		}

		n = len + d;
		if (n > fbufsize) {
			char *s = realloc(fbuf, n);
			if (!s) {
				err = errno;
				break;
			}
			fbuf = s;
			fbufsize = n;
		}

		memcpy(fbuf, path, len);
		fbuf[len] = 0;
		if (stat(fbuf, &st) == 0) {
			if (S_ISDIR(st.st_mode)) {
				strcat(strcat(fbuf, "/"), ldapconffile);
				if (access(fbuf, F_OK) == 0) {
					err = 0;
					break;
				}
			} else if (S_ISREG(st.st_mode)) {
				err = 0;
				break;
			}
		} else if (errno != ENOENT)
			mu_error("can't stat %s: %s", fbuf, strerror(errno));
		path = p;
	}

	if (err == 0) {
		*file_name = fbuf;
		ret = ldap_read_config_file(fbuf);
	} else {
		*file_name = NULL;
	}

	if (err)
		errno = err;

	return ret;
}

static void
argcv_free(int wc, char **wv)
{
	int i;

	for (i = 0; i < wc; i++)
		free(wv[i]);
	free(wv);
}

static int
argcv_split(const char *str, int *pargc, char ***pargv)
{
	int argc, i;
	char **argv;
	const char *p;
	int rc = 0;

	argc = 1;
	for (p = str; *p; p++) {
		if (*p == ' ')
			argc++;
	}
	argv = calloc(argc + 1, sizeof(argv[0]));
	if (!argv)
		return 1;
	for (i = 0, p = str;;) {
		size_t len = strcspn(p, " ");
		char *q = malloc(len + 1);

		if (!q) {
			rc = errno;
			break;
		}
		memcpy(q, p, len);
		q[len] = 0;
		argv[i++] = q;
		p += len;
		if (p)
			p += strspn(p, " ");
		if (!*p)
			break;
	}

	if (rc) {
		argcv_free(argc, argv);
		errno = rc;
		return 1;
	}

	argv[i] = NULL;
	*pargc = argc;
	*pargv = argv;
	return 0;
}

static char *
argcv_concat(int wc, char **wv)
{
	char *res, *p;
	size_t size = 0;
	int i;

	for (i = 0; i < wc; i++)
		size += strlen(wv[i]) + 1;
	res = malloc(size);
	if (!res)
		return 0;
	for (p = res, i = 0;;) {
		strcpy(p, wv[i]);
		p += strlen(wv[i]);
		if (++i < wc)
			*p++ = ' ';
		else
			break;
	}
	*p = 0;
	return res;
}

static char *
parse_ldap_uri(const char *uri)
{
	int wc;
	char **wv;
	LDAPURLDesc *ludlist, **ludp;
	char **urls = NULL;
	int nurls = 0;
	char *ldapuri = NULL;
	int rc;

	rc = ldap_url_parse(uri, &ludlist);
	if (rc != LDAP_URL_SUCCESS) {
		mu_error("cannot parse LDAP URL(s)=%s (%d)", uri, rc);
		return NULL;
	}

	for (ludp = &ludlist; *ludp; ) {
		LDAPURLDesc *lud = *ludp;
		char **tmp;

		if (lud->lud_dn && lud->lud_dn[0]
		    && (lud->lud_host == NULL || lud->lud_host[0] == '\0'))  {
			/* if no host but a DN is provided, try
			   DNS SRV to gather the host list */
			char *domain = NULL, *hostlist = NULL;
			size_t i;

			if (ldap_dn2domain(lud->lud_dn, &domain) || !domain) {
				mu_error("DNS SRV: cannot convert "
				       "DN=\"%s\" into a domain",
				       lud->lud_dn);
				goto dnssrv_free;
			}

			rc = ldap_domain2hostlist(domain, &hostlist);
			if (rc) {
				mu_error("DNS SRV: cannot convert "
				       "domain=%s into a hostlist",
				       domain);
				goto dnssrv_free;
			}

			if (argcv_split(hostlist, &wc, &wv)) {
				mu_error("DNS SRV: could not parse "
				       "hostlist=\"%s\": %s",
				       hostlist, strerror(errno));
				goto dnssrv_free;
			}

			tmp = realloc(urls, sizeof(char *) * (nurls + wc + 1));
			if (!tmp) {
				mu_error("DNS SRV %s", strerror(errno));
				goto dnssrv_free;
			}

			urls = tmp;
			urls[nurls] = NULL;

			for (i = 0; i < wc; i++) {
				char *p = malloc(strlen(lud->lud_scheme) +
						 strlen(wv[i]) +
						 3);
				if (!p) {
					mu_error("DNS SRV %s", strerror(errno));
					goto dnssrv_free;
				}

				strcpy(p, lud->lud_scheme);
				strcat(p, "//");
				strcat(p, wv[i]);

				urls[nurls + i + 1] = NULL;
				urls[nurls + i] = p;
			}

			nurls += i;

		  dnssrv_free:
			argcv_free(wc, wv);
			ber_memfree(hostlist);
			ber_memfree(domain);
		} else {
			tmp = realloc(urls, sizeof(char *) * (nurls + 2));
			if (!tmp) {
				mu_error("DNS SRV %s", strerror(errno));
				break;
			}
			urls = tmp;
			urls[nurls + 1] = NULL;

			urls[nurls] = ldap_url_desc2str(lud);
			if (!urls[nurls]) {
				mu_error("DNS SRV %s", strerror(errno));
				break;
			}
			nurls++;
		}

		*ludp = lud->lud_next;

		lud->lud_next = NULL;
		ldap_free_urldesc(lud);
	}

	if (ludlist) {
		ldap_free_urldesc(ludlist);
		return NULL;
	} else if (!urls)
		return NULL;
	ldapuri = argcv_concat(nurls, urls);
	if (!ldapuri)
		mu_error("%s", strerror(errno));
	ber_memvfree((void **)urls);
	return ldapuri;
}

static int
full_read(int fd, char const *file, char *buf, size_t size)
{
	while (size) {
		ssize_t n;

		n = read(fd, buf, size);
		if (n == -1) {
			if (errno == EAGAIN || errno == EINTR)
				continue;
			mu_error("error reading from %s: %s",
			       file, strerror(errno));
			return -1;
		} else if (n == 0) {
			mu_error("short read from %s", file);
			return -1;
		}

		buf += n;
		size -= n;
	}
	return 0;
}

static int
get_passwd(struct ldap_param *env, struct berval *pwd, char **palloc)
{
	char const *file;

	file = ldap_param_get(env, "bindpwfile");
	if (file) {
		struct stat st;
		int fd, rc;
		char *mem, *p;

		fd = open(file, O_RDONLY);
		if (fd == -1) {
			mu_error("can't open password file %s: %s",
			       file, strerror(errno));
			return -1;
		}
		if (fstat(fd, &st)) {
			mu_error("can't stat password file %s: %s",
			       file, strerror(errno));
			close(fd);
			return -1;
		}
		mem = malloc(st.st_size + 1);
		if (!mem) {
			mu_error("can't allocate memory (%lu bytes)",
			       (unsigned long) st.st_size+1);
			close(fd);
			return -1;
		}
		rc = full_read(fd, file, mem, st.st_size);
		close(fd);
		if (rc)
			return rc;
		mem[st.st_size] = 0;
		p = strchr(mem, '\n');
		if (p)
			*p = 0;
		*palloc = mem;
		pwd->bv_val = mem;
	} else
		pwd->bv_val = ldap_param_get(env, "bindpw");
	pwd->bv_len = pwd->bv_val ? strlen(pwd->bv_val) : 0;
	return 0;
}

static int
get_intval(struct ldap_param *env, const char *name, int base, unsigned long *pv)
{
	char *p;
	char *v = ldap_param_get(env, name);

	if (!v)
		return 1;
	errno = 0;
	*pv = strtoul(v, &p, base);
	if (errno || *p) {
		mu_error("configuration variable %s is not an integer", name);
		return -1;
	}
	return 0;
}

static int
mfmod_ldap_bind(LDAP *ld, struct ldap_param *env)
{
	int msgid, err, rc;
	LDAPMessage *result;
	LDAPControl **ctrls;
	char msgbuf[256];
	char *matched = NULL;
	char *info = NULL;
	char **refs = NULL;
	struct berval passwd;
	char *binddn;
	char *alloc_ptr = NULL;

	binddn = ldap_param_get(env, "binddn");

	if (get_passwd(env, &passwd, &alloc_ptr))
		return 1;

	msgbuf[0] = 0;

	rc = ldap_sasl_bind(ld, binddn, LDAP_SASL_SIMPLE, &passwd,
			    NULL, NULL, &msgid);
	if (msgid == -1) {
		mu_error("ldap_sasl_bind(SIMPLE) failed: %s", ldap_err2string(rc));
		free(alloc_ptr);
		return 1;
	}

	if (ldap_result(ld, msgid, LDAP_MSG_ALL, NULL, &result) == -1) {
		mu_error("ldap_result failed");
		free(alloc_ptr);
		return 1;
	}

	rc = ldap_parse_result(ld, result, &err, &matched, &info, &refs,
			       &ctrls, 1);
	if (rc != LDAP_SUCCESS) {
		mu_error("ldap_parse_result failed: %s", ldap_err2string(rc));
		free(alloc_ptr);
		return 1;
	}

	if (ctrls)
		ldap_controls_free(ctrls);

	if (err != LDAP_SUCCESS
	    || msgbuf[0]
	    || (matched && matched[0])
	    || (info && info[0])
	    || refs) {

		DEBUG(2, "mfmod_ldap_bind: %s (%d)%s",
		      ldap_err2string(err), err, msgbuf);

		if (matched && *matched)
			DEBUG(2 ,"matched DN: %s", matched);

		if (info && *info)
			DEBUG(2, "additional info: %s", info);

		if (refs && *refs) {
			int i;
			DEBUG(3, "%s:", "referrals");
			for (i = 0; refs[i]; i++)
				DEBUG(3,"%s", refs[i]);
		}
	}

	if (matched)
		ber_memfree(matched);
	if (info)
		ber_memfree(info);
	if (refs)
		ber_memvfree((void **)refs);

	free(alloc_ptr);

	return !(err == LDAP_SUCCESS);
}

static void
mfmod_ldap_unbind(LDAP *ld)
{
	if (ld) {
		ldap_set_option(ld, LDAP_OPT_SERVER_CONTROLS, NULL);
		ldap_unbind_ext(ld, NULL, NULL);
	}
}

static LDAP *
mfmod_ldap_connect(struct ldap_param *env)
{
	int rc;
	char *ldapuri = NULL;
	LDAP *ld = NULL;
	int protocol = LDAP_VERSION3;
	char *val;
	unsigned long lval;
	enum { tls_no, tls_yes,	tls_only } tls = tls_no;

	if (ldap_debug_level) {
		if (ber_set_option(NULL, LBER_OPT_DEBUG_LEVEL,
				    &ldap_debug_level)
		    != LBER_OPT_SUCCESS )
			mu_error("cannot set LBER_OPT_DEBUG_LEVEL %d",
			       ldap_debug_level);

		if (ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL,
				     &ldap_debug_level)
		    != LDAP_OPT_SUCCESS )
			mu_error("could not set LDAP_OPT_DEBUG_LEVEL %d",
			       ldap_debug_level);
	}

	val = ldap_param_get(env, "uri");
	if (val) {
		ldapuri = parse_ldap_uri(val);
		if (!ldapuri)
			return NULL;
	}
	DEBUG(1, "connecting to URI: %s", ldapuri ? ldapuri : "<DEFAULT>");

	rc = ldap_initialize(&ld, ldapuri);
	if (rc != LDAP_SUCCESS) {
		mu_error("cannot create LDAP session handle for "
		       "URI=%s (%d): %s",
		       ldapuri, rc, ldap_err2string(rc));
		free(ldapuri);
		return NULL;
	}
	free(ldapuri);

	if (get_intval(env, "ldap_version", 10, &lval) == 0) {
		switch (lval) {
		case 2:
			protocol = LDAP_VERSION2;
			break;
		case 3:
			protocol = LDAP_VERSION3;
			break;
		default:
			mu_error("%s: invalid variable value, "
			       "defaulting to 3",
			       "ldap_version");
		}
	}

	ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &protocol);

	val = ldap_param_get(env, "tls");

	if (val) {
		if (strcmp(val, "yes") == 0)
			tls = tls_yes;
		else if (strcmp(val, "no") == 0)
			tls = tls_no;
		else if (strcmp(val, "only") == 0)
			tls = tls_only;
		else {
			mu_error("wrong value for tls statement, assuming \"no\"");
			tls = tls_no;
		}
	}

	if (tls != tls_no) {
		rc = ldap_start_tls_s(ld, NULL, NULL);
		if (rc != LDAP_SUCCESS) {
			char *msg = NULL;
			ldap_get_option(ld,
					LDAP_OPT_DIAGNOSTIC_MESSAGE,
					(void*)&msg);
			mu_error("ldap_start_tls failed: %s",
			       ldap_err2string(rc));
			mu_error("TLS diagnostics: %s", msg);
			ldap_memfree(msg);

			if (tls == tls_only) {
				mfmod_ldap_unbind(ld);
				return NULL;
			}
			/* try to continue anyway */
		} else {
			val = ldap_param_get(env, "tls_cacert");
			if (val) {
				rc = ldap_set_option(ld,
						     LDAP_OPT_X_TLS_CACERTFILE,
						     val);
				if (rc != LDAP_SUCCESS) {
					mu_error("setting of LDAP_OPT_X_TLS_CACERTFILE failed");
					if (tls == tls_only) {
						mfmod_ldap_unbind(ld);
						return NULL;
					}
				}
			}
		}
	}

	/* FIXME: Timeouts, SASL, etc. */
	return ld;
}

static inline int
eargrange(MFMOD_PARAM *r, int n)
{
	return mfmod_error(r, mfe_range,
			   "argument #%d is out of allowed range",
			   n+1);
}

int
mfmod_ldap_get_config_path(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	ASSERT_ARGCOUNT(r, count, 0);

	r->type = mfmod_string;
	if ((r->string = strdup(ldap_config_path())) == NULL)
		return -1;
	return 0;
}

int
mfmod_ldap_set_config_path(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	char *s;

	ASSERT_ARGCOUNT(r, count, 1);
	ASSERT_ARGTYPE(p, r, 0, mfmod_string);

	if ((s = strdup(p[0].string)) == NULL)
		return -1;

	free(ldapconfpath);
	ldapconfpath = s;

	r->type = mfmod_number;
	r->number = 0;
	return 0;
}

int
mfmod_ldap_set_debug_level(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	switch (count) {
	case 2:
		ASSERT_ARGTYPE(p, r, 1, mfmod_number);
		ldap_debug_level = p[1].number;
		/* fall through */
	case 1:
		ASSERT_ARGTYPE(p, r, 0, mfmod_number);
		debug_level = p[0].number;
		break;

	default:
		return mfmod_error(r, mfe_inval, "%s",
				   "bad number of arguments");
	}

	r->type = mfmod_number;
	r->number = 0;

	return 0;
}

static size_t max_responses;
struct mfmod_response *responses;

static long
alloc_response(void)
{
	size_t i, n;
	for (i = 0; i < max_responses; i++) {
		if (responses[i].ld == NULL)
			goto end;
	}
	n = max_responses;
	responses = mu_2nrealloc(responses, &max_responses, sizeof(responses[0]));
	memset(responses + n, 0, (max_responses - n) * sizeof(responses[0]));
end:
	if (i > LONG_MAX) return -1;
	return i;
}

static void
response_attr_free(struct mfmod_response *resp, int all)
{
	size_t i;

	free(resp->dn);
	resp->dn = NULL;
	for (i = 0; i < resp->nattr; i++) {
		argcv_free(resp->attr[i].count, resp->attr[i].values);
		resp->attr[i].count = 0;
		resp->attr[i].values = NULL;
		if (all)
			free(resp->attr[i].name);
	}
}

struct mfmod_response *
response_get(int n)
{
	struct mfmod_response *resp = NULL;
	if (n >= 0 && n < max_responses) {
		resp = responses + n;
		if (!resp->ld)
			return NULL;
	}
	return resp;
}

void
free_response(long n)
{
	struct mfmod_response *resp = &responses[n];

	if (resp->res)
		ldap_msgfree(resp->res);
	if (resp->ld)
		mfmod_ldap_unbind(resp->ld);

	response_attr_free(resp, 1);
	free(resp->attr);
	memset(resp, 0, sizeof(*resp));
}

struct mfmod_response_attr *
response_attr_get(struct mfmod_response *resp, char const *name)
{
	size_t i;

	for (i = 0; i < resp->nattr; i++)
		if (strcmp(resp->attr[i].name, name) == 0)
			return &resp->attr[i];
	return NULL;
}

static int
response_attr_fill(struct mfmod_response *resp, struct mfmod_response_attr *attr)
{
	LDAP *ld = resp->ld;
	LDAPMessage *msg = resp->cur;
	int i, count;
	struct berval **values;
	int ret = 0;

	values = ldap_get_values_len(ld, msg, attr->name);
	if (!values) {
		DEBUG(2, "%s: LDAP attribute `%s' has NULL value",
		      resp->dn, attr->name);
		attr->count = 0;
		attr->values = NULL;
		return 0;
	}

	for (count = 0; values[count]; count++)
		;

	attr->values = calloc(count + 1, sizeof(attr->values[0]));
	if (!attr->values) {
		mu_error("%s", strerror(errno));
		ret = -1;
	} else {
		for (i = 0; values[i]; i++) {
			char *p = malloc(values[i]->bv_len + 1);
			if (!p) {
				mu_error("%s", strerror(errno));
				break;
			}
			memcpy(p, values[i]->bv_val, values[i]->bv_len);
			p[values[i]->bv_len] = 0;
			attr->values[i] = p;
			DEBUG(10, "value %d: %s", i, p);
		}

		attr->count = i;
	}

	ldap_value_free_len(values);
	return ret;
}

static int
response_fill(struct mfmod_response *resp)
{
	size_t i;

	response_attr_free(resp, 0);
	if (resp->cur) {
		resp->dn = ldap_get_dn(resp->ld, resp->cur);
		for (i = 0; i < resp->nattr; i++) {
			if (response_attr_fill(resp, &resp->attr[i]))
				return -1;
		}
	}
	return 0;
}

// search(base, filter, attrs...)
int
mfmod_ldap_search(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	struct ldap_param *env;
	LDAP *ld;
	char **attr;
	size_t nattr;
	int rc = 0;
	ber_int_t msgid;
	char *base;
	char *filter;
	long i;
	char *conf_name;
	long rn;
	struct mfmod_response *resp = NULL;

	/* Check parameter consistency */
	if (count < 3)
		return mfmod_error(r, mfe_inval, "%s",
				   "bad number of arguments");

	for (i = 0; i < count; i++)
		ASSERT_ARGTYPE(p, r, i, mfmod_string);

	/* Read configuration */
	env = mfmod_ldap_conf_parse(&conf_name);
	if (env == NULL) {
		if (errno == ENOENT) {
			return mfmod_error(r, mfe_not_found,
					   "configuration file %s not found in path %s",
					   ldapconffile,
					   ldap_config_path());
		} else {
			int rc = mfmod_error(r, mfe_failure,
					     "error reading configuration file %s: %s",
					     conf_name,
					     mu_strerror(errno));
			free(conf_name);
			return rc;
		}
	}

	base = p[0].string[0] != 0 ? p[0].string : ldap_param_get(env, "base");
	filter = p[1].string;

	ld = mfmod_ldap_connect(env);
	if (!ld)
		return -1;
	if (mfmod_ldap_bind(ld, env)) {
		mfmod_ldap_unbind(ld);
		return -1;
	}

	if ((rn = alloc_response()) == -1) {
		mu_error("ldap response table is full");
		mfmod_ldap_unbind(ld);
		return -1;
	}
	resp = &responses[rn];
	resp->ld = ld;

	nattr = count - 2;
	attr = mu_calloc(nattr + 1, sizeof(attr[0]));
	for (i = 0; i < nattr; i++)
		attr[i] = p[i+2].string;
	attr[i] = NULL;

	rc = ldap_search_ext(resp->ld, base, LDAP_SCOPE_SUBTREE,
			     filter, attr, 0,
			     NULL, NULL, NULL, -1, &msgid);
	free(attr);

	if (rc != LDAP_SUCCESS) {
		mu_error("ldap_search_ext: %s", ldap_err2string(rc));
		r->type = mfmod_number;
		r->number = rc;
		rc = -1;
		goto end;
	}

	rc = ldap_result(ld, msgid, LDAP_MSG_ALL, NULL, &resp->res);
	if (rc < 0) {
		mu_error("ldap_result failed");
		rc = -1;
		goto end;
	}

	if ((resp->cur = ldap_first_entry(resp->ld, resp->res)) == NULL) {
		resp->nattr = 0;
		rc = 0;
	} else {
		resp->attr = mu_calloc(nattr, sizeof(resp->attr[0]));
		for (i = 0; i < nattr; i++) {
			resp->attr[i].name = mu_strdup(p[i+2].string);
		}
		resp->nattr = i;
		rc = response_fill(resp);
	}
end:
	if (rc) {
		free_response(rn);
		rn = -1;
	}
	ldap_param_free(env);

	r->type = mfmod_number;
	r->number = rn;

	return rc;
}

int
mfmod_ldap_search_result_free(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	ASSERT_ARGCOUNT(r, count, 1);
	ASSERT_ARGTYPE(p, r, 0, mfmod_number);
	if (!response_get(p[0].number))
		return eargrange(r, 0);
	free_response(p[0].number);
	r->type = mfmod_number;
	r->number = 0;
	return 0;
}

int
mfmod_ldap_search_result_count_entries(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	struct mfmod_response *resp;
	ASSERT_ARGCOUNT(r, count, 1);
	ASSERT_ARGTYPE(p, r, 0, mfmod_number);
	if ((resp = response_get(p[0].number)) == NULL)
		return eargrange(r, 0);
	r->type = mfmod_number;
	r->number = ldap_count_entries(resp->ld, resp->res);
	return 0;
}

int
mfmod_ldap_search_result_next_entry(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	struct mfmod_response *resp;
	ASSERT_ARGCOUNT(r, count, 1);
	ASSERT_ARGTYPE(p, r, 0, mfmod_number);
	if ((resp = response_get(p[0].number)) == NULL)
		return eargrange(r, 0);
	if (resp->cur) {
		resp->cur = ldap_next_entry(resp->ld, resp->cur);
		response_fill(resp);
	}
	r->type = mfmod_number;
	r->number = resp->cur != NULL;
	return 0;
}

int
mfmod_ldap_search_result_dn(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	struct mfmod_response *resp;
	ASSERT_ARGCOUNT(r, count, 1);
	ASSERT_ARGTYPE(p, r, 0, mfmod_number);
	if ((resp = response_get(p[0].number)) == NULL)
		return eargrange(r, 0);
	r->type = mfmod_string;
	r->string = mu_strdup(resp->dn);
	return 0;
}

int
mfmod_ldap_search_result_count_attrs(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	struct mfmod_response *resp;
	ASSERT_ARGCOUNT(r, count, 1);
	ASSERT_ARGTYPE(p, r, 0, mfmod_number);
	if ((resp = response_get(p[0].number)) == NULL)
		return eargrange(r, 0);
	r->type = mfmod_number;
	r->number = resp->nattr;
	return 0;
}

int
mfmod_ldap_search_result_attr_name(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	struct mfmod_response *resp;
	long n;

	ASSERT_ARGCOUNT(r, count, 2);
	ASSERT_ARGTYPE(p, r, 0, mfmod_number);
	if ((resp = response_get(p[0].number)) == NULL)
		return eargrange(r, 0);
	ASSERT_ARGTYPE(p, r, 1, mfmod_number);
	if (!((n = p[1].number) >= 0 && n < resp->nattr))
		return eargrange(r, 1);
	r->type = mfmod_string;
	r->string = strdup(resp->attr[n].name);
	return 0;
}


int
mfmod_ldap_search_result_attr_value_count(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	struct mfmod_response *resp;
	struct mfmod_response_attr *attr;

	ASSERT_ARGCOUNT(r, count, 2);
	ASSERT_ARGTYPE(p, r, 0, mfmod_number);
	if ((resp = response_get(p[0].number)) == NULL)
		return eargrange(r, 0);
	ASSERT_ARGTYPE(p, r, 1, mfmod_string);

	attr = response_attr_get(resp, p[1].string);
	if (attr)
		r->number = attr->count;
	else
		r->number = 0;
	r->type = mfmod_number;
	return 0;
}

int
mfmod_ldap_search_result_attr_value_get(long count, MFMOD_PARAM *p, MFMOD_PARAM *r)
{
	struct mfmod_response *resp;
	struct mfmod_response_attr *attr;

	ASSERT_ARGCOUNT(r, count, 3);
	ASSERT_ARGTYPE(p, r, 0, mfmod_number);
	if ((resp = response_get(p[0].number)) == NULL)
		return eargrange(r, 0);
	ASSERT_ARGTYPE(p, r, 1, mfmod_string);
	ASSERT_ARGTYPE(p, r, 2, mfmod_number);
	if (p[2].number < 0)
		return eargrange(r, 2);
	attr = response_attr_get(resp, p[1].string);
	if (attr) {
		if (p[2].number >= attr->count)
			return eargrange(r, 2);
		r->string = strdup(attr->values[p[2].number]);
	} else
		return eargrange(r, 1);
	r->type = mfmod_string;
	return 0;
}
